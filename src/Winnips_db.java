import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Winnips_db {
	
	public static final String DBFILE = "Winnips.db";
	public static final String CSVFILE = "Winnips.csv";
			
	public List<List<String>> readCSV(String fName) throws FileNotFoundException {
		 
		 List<List<String>> records = new ArrayList<>();
		 try (Scanner scan = new Scanner(new File (fName))) {			 
			 while(scan.hasNextLine()) {
				 String line = scan.nextLine();
				 if (line.length() > 0  ) {
					 records.add(extractColumns(line));

				 }
			 }			 
		 }
		 return records;
	 }
	 
	public List<String> extractColumns(String line){
		
		List<String> record = new ArrayList<String>();
		try (Scanner scan = new Scanner(line)) {
			scan.useDelimiter(";");
			
			while(scan.hasNext()) {
				record.add(scan.next().replaceAll("[^A-Za-z0-9_\"]", ""));
			}
		}
		return record;		
	}	
	
	public void prepareDb (String path, List<List<String>> records) throws SQLException {
		try (Connection con = DriverManager.getConnection("jdbc:sqlite:"+path)){
			DatabaseMetaData meta = con.getMetaData();
            System.out.println("The driver name is " + meta.getDriverName());
            System.out.println("A new database has been created.");
            Statement smnt = con.createStatement();
            
            List<String> header = records.get(0);
            List<String> data = records.get(1);
            
            String createTable = "CREATE TABLE IF NOT EXISTS DivorcesByAge (";
            for (int i = 0; i < header.size(); i++) {
              if (i > 0) {
                createTable += ", ";
              }
              String h = header.get(i).strip();
              String d = data.get(i).strip();
              // Is Text
              createTable += h.substring(1, h.length() - 1);
              if (d.startsWith("\"")) {
                createTable += " TEXT"; 
              } else {
                createTable += " INT";
              }
            }
            createTable += ");";
            System.out.println("Dropping table DivorcesByAge");
            smnt.execute("DROP TABLE IF EXISTS DivorcesByAge;");
            System.out.println(createTable);
            smnt.execute(createTable);
            System.out.println("Table created or existing");
		}
		
		System.out.println("Connection closed");

	}
	
	public int countRecords (String path) throws SQLException {
		try (Connection con = DriverManager.getConnection("jdbc:sqlite:"+path)){
			Statement smnt = con.createStatement();
			ResultSet rs = smnt.executeQuery("SELECT COUNT(*) AS count FROM DivorcesByAge;");
			return rs.getInt(1);			
		}
		
	}
	
	public void insertRecords (String path, List<List<String>> records) throws SQLException {
		try (Connection con = DriverManager.getConnection("jdbc:sqlite:"+path)){
			PreparedStatement psmt = con.prepareStatement("INSERT INTO DivorcesByAge (YEAR, CANTON, AGE_HUSBAND, AGE_WIFE, OBS_VALUE) VALUES (?, ?, ?, ?, ?);");
			System.out.println("Statement prepared");
			int n = 0;
			records.remove(0);
			for(List<String> record : records) {
				psmt.setInt(1, Integer.parseInt(record.get(0)));
				psmt.setString(2, record.get(1));
				psmt.setString(3, record.get(2));
				psmt.setString(4, record.get(3));
				psmt.setInt(5, Integer.parseInt(record.get(4)));
				psmt.executeUpdate();
				n++;
			}
			
			System.out.println("Inserted "+ n + " records");
			
		}
		System.out.println("Connection closed");
	}
	
	
	public static void main(String[] args) throws FileNotFoundException, SQLException {
		
		
		Winnips_db test = new Winnips_db();
		List<List<String>> records = test.readCSV(Winnips_db.CSVFILE);
		System.out.println(records.size());
		test.prepareDb(Winnips_db.DBFILE,records);
		test.insertRecords(Winnips_db.DBFILE,records);
		System.out.println(test.countRecords(Winnips_db.DBFILE));
		

		
		
	}
}
