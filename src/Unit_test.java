import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.sql.SQLException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Unit_test {
	@Test
	void testLineCountOfCsvReading() throws FileNotFoundException {
		Winnips_db test = new Winnips_db();
		int n = test.readCSV(Winnips_db.CSVFILE).size();
		assertEquals(12910,n,"Line count does not match");		
	}
	@Test
	void testLineCountOfDbTable() throws SQLException {
		Winnips_db test = new Winnips_db();
		int n = test.countRecords(Winnips_db.DBFILE);
		assertEquals(12910,n,"Line count does not match");		
	}

}
